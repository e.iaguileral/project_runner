using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class enemy_spawner : MonoBehaviour
{
    public GameObject normal_enemy;
    public GameObject spiny_enemy;
    public GameObject shooter_enemy;

    public static bool areShooterEnemy = false;
    public bool spawnBlock = false;
    public float stageTime;
    void Start()
    {
        stageTime = 1;
        StartCoroutine(spawn());
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void spawnEnemy(GameObject newgo) {
        float randY = Random.Range(-4, -1);
        if (randY == -4) { randY = -3.7f; } else if (randY == -3) { randY = -2.7f; }
        newgo.transform.position = new Vector2(10, randY);
        player.areEnemies++;
    }

    public IEnumerator spawn() {
        while (true) {
            if (!spawnBlock) {
                if (player.stage == 1)
                {
                    stageTime = 1;
                    GameObject newgo;
                    int randEnemy = Random.Range(0, 4);
                    if (randEnemy == 1 || randEnemy == 2 || randEnemy == 3 )
                    {
                        newgo = Instantiate(normal_enemy);
                        spawnEnemy(newgo);
                    }
                }
                else if (player.stage == 2) {
                    stageTime = 1;
                    GameObject newgo;
                    int randEnemy = Random.Range(0, 3);
                    if (randEnemy == 1)
                    {
                        newgo = Instantiate(normal_enemy);
                        spawnEnemy(newgo);
                    }
                    else if (randEnemy == 2)
                    {
                        newgo = Instantiate(spiny_enemy);
                        spawnEnemy(newgo);
                    }
                }
                else if (player.stage == 3)
                {
                    stageTime = 1;
                    GameObject newgo;
                    int randEnemy = Random.Range(0, 6);
                    if (randEnemy == 1 || randEnemy == 2)
                    {
                        newgo = Instantiate(normal_enemy);
                        spawnEnemy(newgo);
                    }
                    else if (randEnemy == 3 || randEnemy == 4)
                    {
                        newgo = Instantiate(spiny_enemy);
                        spawnEnemy(newgo);
                    }
                    else if (randEnemy == 5 && areShooterEnemy == false) {
                        newgo = Instantiate(shooter_enemy);
                        areShooterEnemy = true;
                        float randY = Random.Range(-4, -1);
                        if (randY == -4) { randY = -3.5f; } else if (randY == -3) { randY = -2.7f; }
                        newgo.transform.position = new Vector2(8, randY);
                        player.areEnemies++;
                    }
                }
            }
            yield return new WaitForSeconds(stageTime);
        }
       
    }
}
