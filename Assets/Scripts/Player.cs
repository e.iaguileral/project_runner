using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using TMPro;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class player : MonoBehaviour {
    //Movimiento:
    static bool canjump = true;
    private bool canmovecamera = false;
    public static bool canstrike = true;
    private bool movePlayer = false;
    private Vector2 originialPlayerPosition;
    private Vector3 originialCameraPosition;

    //Progresi�n:
    static float time = 0;
    public static bool actualShop = false;
    private bool specialRoom = false;
    public static bool boss = false;
    public static bool final = false;
    public static int stage = 1;
    public static int areEnemies = 0;

    //Estad�sticas
    public static int maxLife = 3;
    public static int life = 3;
    public static int coin = 0;

    //Audio:
    public AudioClip musicBoss;
    private AudioSource audio;
    public AudioClip[] audios;

    //Generaci�n de salas:
    private string upExitValue;
    private string downExitValue;
    private String[] rooms = { "heal", "money", "object","shop"};
    private List<string> objectList = new List<string>() { "metalpunch", "lifeup", "moneyup", "rangeup","laserpower","timerstage","repeat","vampire","descuento"};

    //Objetos obtenibles:
    public static int moneyup = 1;
    public static bool metalpunch = false;
    public static int laserpower = 0;
    public static float timerstage = 1;
    public static bool descuento = false;
    public static bool vampire = false;
    public static float range = 0f;

    //Elementos usados:
    public Sprite[] arraySprite;
    public Sprite[] arrows;
    public List<GameObject> livesInCanvas;
    private List<GameObject> specialObjectList = new List<GameObject>();
    public enemy_spawner espawner;
    public GameObject descripcion;
    public GameObject[] precios;
    public GameObject credits;
    public GameObject arrowUp;
    public GameObject arrowDown;
    public GameObject live;
    public GameObject bullet;
    public GameObject newStage;
    public GameObject specialObject;
    public GameObject camera;
    public GameObject strike;
    public GameObject boss_enemy;

    private Animator playerAnimator;
    private Rigidbody2D rb;
    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        originialPlayerPosition = GetComponent<Transform>().position;
        originialCameraPosition = camera.GetComponent<Transform>().position;

        audio = GetComponent<AudioSource>();
        audio.loop = false;
        
        arrowUp.SetActive(false);
        arrowDown.SetActive(false);
        newStage.SetActive(false);
        precios[0].SetActive(false);
        precios[1].SetActive(false);
        precios[2].SetActive(false);
        descripcion.SetActive(false);

        randomRooms();
        changeHeart();
    }

    void Update()
    {
        //Acciones b�sicas:
        playerdead();
        playerjump();
        if (specialRoom == false || boss)
        {
            hit();
        }

        if (specialRoom == false)
        {
            //Acciones para determinar la progresi�n del nivel:
            time += Time.deltaTime;
            terminarStage();
            if (camera.transform.position.x < 10 && canmovecamera)
            {
                moveCamera();
            }
        }
        else {
            newStage.SetActive(true);
        }        

        if (spiny_enemy.playerTakeDamage) {
            spiny_enemy.playerTakeDamage = false;
            changeHeart();
        }
        if (movePlayer)
        {
            rb.velocity = new Vector2(6f, rb.velocity.y);
        }
        if (final) {
            camera.GetComponent<AudioSource>().Stop();
            credits.SetActive(true);
        }
    }

    //Funci�n para mover la c�mara, solo para el final de cada nivel:
    void moveCamera()
    {
        camera.GetComponent<Transform>().position = new Vector3(this.transform.position.x + 5.62f, camera.transform.position.y, -10);
    }

    void playerjump() {
        if (canjump && canstrike)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                rb.AddForce(new Vector2(0, 350));
                canjump = false;
            }
        }
    }

    void hit() {
        if (canstrike) {
            //Ataque normal:
            if (Input.GetKeyDown(KeyCode.Space)) {
                playerAnimator.SetBool("atack", true);
                Instantiate(strike, new Vector2(rb.transform.position.x + 0.5f + range, rb.transform.position.y), transform.rotation);
                canstrike = false;
                rb.velocity = new Vector2(rb.velocity.x, 0);
                StartCoroutine(timeAttack(0.4f));
            }
            //Ataque bajo:
            else if (Input.GetKeyDown(KeyCode.DownArrow) && canjump) {
                playerAnimator.SetBool("atackDown", true);
                Instantiate(strike, new Vector2(rb.transform.position.x + 0.3f, rb.transform.position.y - 0.9f), transform.rotation);
                canstrike = false;
                GetComponent<BoxCollider2D>().offset = new Vector2(-0.07789977f, -0.119234f);
                GetComponent<BoxCollider2D>().size = new Vector2(0.220769f, 0.1501022f);
                StartCoroutine(timeAttack(0.5f));
            }
        }
    }

    //Corrutina para volver a atacar, el tiempo depende del ataque hecho:
    IEnumerator timeAttack(float time) {
        yield return new WaitForSeconds(time);
        canstrike = true;

        playerAnimator.SetBool("atack", false);
        playerAnimator.SetBool("atackDown", false);

        GetComponent<BoxCollider2D>().offset = new Vector2(-0.07789977f, -0.05433462f);
        GetComponent<BoxCollider2D>().size = new Vector2(0.220769f, 0.2799009f);
    }

    //Funci�n de morir:
    void playerdead()
    {
        if (life <= 0)
        {
            if (!final) {
                SceneManager.LoadScene("gameover");
                Destroy(this.gameObject);
            }
           
        }
    }

    //Funci�n que se activa al terminar el tiempo y los enemigos de un nivel:
    void terminarStage() {
        if (time >= 30 * timerstage) {
            espawner.spawnBlock = true;

            if (areEnemies == 0) {
                movePlayer = true;
                canmovecamera = true;

                arrowDown.SetActive(true);
                arrowUp.SetActive(true);
                parallax.canparallax = false;
            }
        }
    }

    //Funci�n para generar habitaciones aleatorias:
    void randomRooms() {
        int randUp = 0;
        int randDown = 0;
        while (randUp == randDown)
        {
            randUp = UnityEngine.Random.Range(0, 4);
            randDown = UnityEngine.Random.Range(0, 4);
        }

        upExitValue = rooms[randUp];
        arrowUp.GetComponent<SpriteRenderer>().sprite = arrows[randUp];
        downExitValue = rooms[randDown];
        arrowDown.GetComponent<SpriteRenderer>().sprite = arrows[randDown];
    }

    //Funci�n para cambiar a sala especial:
    void changeSpecialRoom() {
        specialRoom = true;
        movePlayer = true;
        rb.transform.position = new Vector2(originialPlayerPosition.x - 5, originialPlayerPosition.y);
        camera.GetComponent<Transform>().position = originialCameraPosition;

        arrowUp.SetActive(false);
        arrowDown.SetActive(false); 
    }

    //Funci�n para crear los objetos especiales:
    void createSpecialObject(String value) {
        if (value != "shop")
        {
            //Generaci�n de un solo objeto sin tienda:
            if (value == "object")
            {
                value = generateObject();
            }

            specialObject.transform.tag = value;
            changeObjectSprite(value, specialObject);
            GameObject instantiateObject = Instantiate(specialObject);
            specialObjectList.Add(instantiateObject);
        }
        else {
            //Generaci�n de tres objetos de tienda:
            actualShop = true;

            GameObject shopObject1 = Instantiate(specialObject, new Vector2(specialObject.transform.position.x-4, specialObject.transform.position.y), transform.rotation);
            shopObject1.transform.tag = "heal";
            changeObjectSprite("heal", shopObject1);

            GameObject shopObject2 = Instantiate(specialObject);
            value = generateObject();
            shopObject2.transform.tag = value;
            changeObjectSprite(value, shopObject2);

            GameObject shopObject3 = Instantiate(specialObject, new Vector2(specialObject.transform.position.x + 4, specialObject.transform.position.y), transform.rotation);
            value = generateObject();
            shopObject3.transform.tag = value;
            changeObjectSprite(value, shopObject3);

            precios[0].SetActive(true);
            precios[1].SetActive(true);
            precios[2].SetActive(true);

            specialObjectList.Add(shopObject1);
            specialObjectList.Add(shopObject2);
            specialObjectList.Add(shopObject3);
        }
        
    }

    //Funci�n para generar uno de los objetos aleatorios, devuelve el tag del objeto generado:
    String generateObject() {
        int index = UnityEngine.Random.Range(0, objectList.Count);
        string value = objectList[index];
        objectList.RemoveAt(index);
        return value;
    }

    //Funci�n para cambiar el sprite de cada objeto:
    void changeObjectSprite(String value, GameObject specialObject) {
        if (value == "money")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[0];
        }
        else if (value == "heal")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[1];
        }
        else if (value == "metalpunch")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[2];
        }
        else if (value == "lifeup")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[10];
        }
        else if (value == "moneyup")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[3];
        }
        else if (value == "rangeup")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[4];
        }
        else if (value == "laserpower")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[5];
        }
        else if (value == "timerstage")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[6];
        }
        else if (value == "repeat")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[7];
        }
        else if (value == "vampire")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[8];
        }
        else if (value == "descuento")
        {
            specialObject.GetComponent<SpriteRenderer>().sprite = arraySprite[9];
        }
    }

    //Funci�n para iniciar nivel despu�s de una sala especial:
    void changeNextStage() {
        //Cambio del nivel:
        time = 0;
        stage++;
        parallax.canparallax = true;

        //Cambio de movimiento:
        movePlayer = false;
        rb.transform.position = originialPlayerPosition;
        rb.velocity = new Vector2(0, 0);
        canmovecamera = false;

        //Cambio de salas especiales:
        specialRoom = false;
        actualShop = false;
        espawner.spawnBlock = false;
        destroyObjectList(specialObjectList);
        randomRooms();

        newStage.SetActive(false);
        precios[0].SetActive(false);
        precios[1].SetActive(false);
        precios[2].SetActive(false);
        descripcion.SetActive(false);

        //Generaci�n del boss en el cuarto nivel:
        if (stage == 4) {
            Instantiate(boss_enemy, new Vector2(6.43f, -3.08f), transform.rotation);
            boss = true;
            specialRoom = true;
            espawner.spawnBlock = false;

            camera.GetComponent<AudioSource>().clip = musicBoss;
            camera.GetComponent<AudioSource>().Play();
        }        
    }

    //Funci�n para cambiar los corazones del Canvas, primero destruye los que hab�a y luego agrega los nuevos:
    void changeHeart()
    {
        destroyObjectList(livesInCanvas);

        Canvas canvas = FindObjectOfType<Canvas>();
        for (int i = 0; i < life; i++)
        {
            GameObject newlive = Instantiate(live, new Vector2(-814 + (220 * i),393), transform.rotation);
            newlive.transform.SetParent(canvas.transform, false);
            livesInCanvas.Add(newlive);
        }
    }

    //Funci�n para destruir objetos de la escena, usada en los corazones y en los objetos especiales:
    void destroyObjectList(List<GameObject> list) {
        if (list != null)
        {
            foreach (GameObject objectInList in list)
            {
                Destroy(objectInList);
            }
        }
    }

    

    //Colisiones:
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "wall")
        {
            rb.transform.position = new Vector2(rb.transform.position.x, rb.transform.position.y - 1); ;
        }
        if (collision.transform.tag == "field")
        {
            canjump = true;
        }
        if (collision.transform.tag == "saw")
        {
            life--;
            playerdead();
            Destroy(collision.gameObject);
            changeHeart();
            audio.clip = audios[1];
            audio.Play();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "enemy")
        {
            life--;
            playerdead();
            Destroy(collision.gameObject);
            areEnemies--;
            audio.clip = audios[1];
            audio.Play();
            changeHeart();
        }
        if (collision.transform.tag == "bullet")
        {
            life--;
            playerdead();
            audio.clip = audios[1];
            audio.Play();
            Destroy(collision.gameObject);
            changeHeart();
        }
        if (collision.transform.tag == "bouncer")
        {
            canjump = false;
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(new Vector2(0, 650));
        }
        if (collision.transform.tag == "downexit")
        {
            changeSpecialRoom();
            createSpecialObject(downExitValue);
        }
        if (collision.transform.tag == "upexit")
        {
            changeSpecialRoom();
            createSpecialObject(upExitValue);

        }
        if (collision.transform.tag == "newstage") {
            changeNextStage();
        }

        //Colisi�n con objetos especiales:
        if (collision.transform.tag == "heal") {
            if (actualShop == false || (actualShop && (coin >= 10 || descuento && coin >= 5))) {
                maxLife++;
                if (maxLife > 8) {
                    maxLife = 8;
                }
                life = maxLife;
                print("Has obtenido un coraz�n");
                Destroy(collision.gameObject);
                changeHeart();
                shopping(10);
            }
        }
        if (collision.transform.tag == "money") {
            coin += 20 * moneyup;
            moneytext.takeMoney = true;
            Destroy(collision.gameObject);
        }
        if (collision.transform.tag == "moneyup") {

            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.SetActive(true);
                descripcion.GetComponent<TextMeshProUGUI>().text = "CONSIGUE EL DOBLE DE MONEDAS";
                moneyup = 2;
                Destroy(collision.gameObject);
                shopping(20);
            }
        }
        if (collision.transform.tag == "metalpunch") {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.SetActive(true);
                descripcion.GetComponent<TextMeshProUGUI>().text = "GOLPEA OBJETOS PUNZANTES";
                metalpunch = true;
                Destroy(collision.gameObject);
                shopping(20);
            }
        }
        if (collision.transform.tag == "rangeup") {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.SetActive(true);
                descripcion.GetComponent<TextMeshProUGUI>().text = "M�S RANGO DE ATAQUE";
                range = 1f;
                Destroy(collision.gameObject);
                shopping(20);
            }
        }
        if (collision.transform.tag == "lifeup") {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.SetActive(true);
                descripcion.GetComponent<TextMeshProUGUI>().text = "VIDA M�XIMA AUMENTADA EN 2";
                maxLife += 2;
                if (maxLife > 8)
                {
                    maxLife = 8;
                }
                life = maxLife;
                Destroy(collision.gameObject);
                changeHeart();
                shopping(20);
            }
        }
        if (collision.transform.tag == "laserpower")
        {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.SetActive(true);
                descripcion.GetComponent<TextMeshProUGUI>().text = "M�S PODER DE L�SER";
                laserpower = 1;
                Destroy(collision.gameObject);
                shopping(20);
            }
        }
        if (collision.transform.tag == "timerstage")
        {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.SetActive(true);
                descripcion.GetComponent<TextMeshProUGUI>().text = "NIVELES DURAN MENOS";
                timerstage = 0.5f;
                Destroy(collision.gameObject);
                shopping(20);
            }
        }
        if (collision.transform.tag == "repeat")
        {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.GetComponent<TextMeshProUGUI>().text = "REPITES EL NIVEL";
                descripcion.SetActive(true);
                stage--;
                Destroy(collision.gameObject);
                shopping(20);
            }
        }
        if (collision.transform.tag == "vampire")
        {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)))
            {
                descripcion.GetComponent<TextMeshProUGUI>().text = "PROBABILIDAD DE CURAR AL GOLPEAR";
                descripcion.SetActive(true);
                vampire = true;
                Destroy(collision.gameObject);
                shopping(20);
            }
        }
        if (collision.transform.tag == "descuento")
        {
            if (actualShop == false || (actualShop && (coin >= 20 || descuento && coin >= 10)) )
            {
                descripcion.SetActive(true);
                descripcion.GetComponent<TextMeshProUGUI>().text = "LOS OBJETOS CUESTAN LA MITAD";
                shopping(20);
                descuento = true;
                precios[0].GetComponent<TextMeshProUGUI>().text = "5$";
                precios[1].GetComponent<TextMeshProUGUI>().text = "10$";
                precios[2].GetComponent<TextMeshProUGUI>().text = "10$";
                Destroy(collision.gameObject);
                
            }
        }

        //Funci�n de tienda
        void shopping(int num)
        {
            if (actualShop)
            {
                if (descuento){
                    coin -= num / 2;
                }
                else{
                    coin -= num;
                }

                moneytext.takeMoney = true;

                audio.clip = audios[0];
                audio.Play();
            }
        }

    }
    
}
