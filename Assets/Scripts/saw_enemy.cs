using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class saw_enemy : MonoBehaviour
{
    private Rigidbody2D rb;
    void Start()
    {
      rb = GetComponent<Rigidbody2D>();
      
    }

    void Update()
    {
        rb.velocity = new Vector2(-6, rb.velocity.y);
        destroyLeft();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "field")
        {
            rb.AddForce(new Vector2(0, 300));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Colisi�n con golpe:
        if (collision.transform.tag == "strike")
        {
            //El jugador recibe da�o si no tiene la mejora de pu�o de metal:
            if (!player.metalpunch)
            {
                player.life--;
                spiny_enemy.playerTakeDamage = true;
                Destroy(this.gameObject);
            }
            else {
                Destroy(this.gameObject);
            }
            
        }
    }

    //Funci�n para destruir el objeto si se pasa de cierto punto del lado izquiero:
    private void destroyLeft()
    {
        if (rb.transform.position.x <= -10)
        {
            Destroy(this.gameObject);
        }
    }
}
