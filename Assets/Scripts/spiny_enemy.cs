using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spiny_enemy : MonoBehaviour
{
    
    bool spike;

    public static bool playerTakeDamage = false;

    public Sprite sprite_spike;
    public Sprite sprite_normal;
    public player player;
    private Rigidbody2D rb;
    void Start()
    {
        if (UnityEngine.Random.Range(0, 2) == 1)
        {
            spike = true;
        }
        else {
            spike = false;
        }

        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-9 + (player.stage*-1), rb.velocity.y);
        StartCoroutine(changeSpike());

    }

    void Update()
    {
        changeSprite();
        destroyLeft();
        
    }

    //Funci�n para cambiar el sprite dependiendo del estado:
    void changeSprite() {
        if (spike)
        {
            GetComponent<SpriteRenderer>().sprite = sprite_spike;
        }
        else {
            GetComponent<SpriteRenderer>().sprite = sprite_normal;
        }
    }

    //Corrutina para cambiar el estado del enemigo:
    public IEnumerator changeSpike() {
        while (true) {
            if (spike)
            {
                spike = false;
            }
            else
            {
                spike = true;
            }
            yield return new WaitForSeconds(1.0f);
        }
    }

   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //El enemigo puede ser golpeado o disparado con el l�ser mejorado:
        if (collision.transform.tag == "strike" || (collision.transform.tag == "returnlaser" && player.laserpower == 1))
        {
            //El enemigo muere si no est� en el estado puntiagudo, si tienes la mejora del pu�o de metal, o si es un l�ser:
            if (!spike || player.metalpunch || (collision.transform.tag == "returnlaser" && player.laserpower == 1))
            {
                player.coin += 1*player.moneyup;
                moneytext.takeMoney = true;

                player.areEnemies--;

                //Si ha sido un golpe y tienes el objeto de vampiro:
                if (collision.transform.tag == "strike" && player.vampire)
                {
                    if (player.life < player.maxLife)
                    {
                        int numr = UnityEngine.Random.Range(1, 8);
                        if (numr == 1)
                        {
                            player.life++;
                            playerTakeDamage = true;
                        }
                    }
                        
                }   
                Destroy(this.gameObject);
                
                
            }
            else {
                player.areEnemies--;
                player.life--;
                playerTakeDamage = true;
                Destroy(this.gameObject);
            }
            
        }
    }

    //Funci�n para destruir el objeto si se pasa de cierto punto del lado izquiero:
    private void destroyLeft()
    {
        if (rb.transform.position.x <= -10)
        {
            player.areEnemies--;
            Destroy(this.gameObject);
        }
    }
}
