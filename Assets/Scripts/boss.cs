using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class boss : MonoBehaviour
{
    public int bossLife = 30;
    public GameObject bullet;
    public GameObject saw;
    void Start()
    {
        StartCoroutine(shootSaw());
        StartCoroutine(shootBullet());
    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "returnlaser")
        {
            bossLife -= 1 + player.laserpower;
            print(bossLife);
            Destroy(collision.gameObject);

            if (bossLife <= 0) {
                player.final = true;
                Destroy(this.gameObject);
            }
        }
    }

    //Corrutina de ataque normal:
    public IEnumerator  shootBullet()
    {
        while (true)
        {
            float randY = UnityEngine.Random.Range(-4, -1);
            if (randY == -4) { randY = -3.5f; } else if (randY == -3) { randY = -2.7f; }
            Instantiate(bullet, new Vector2(GetComponent<Rigidbody2D>().transform.position.x-2f, randY), transform.rotation);
            yield return new WaitForSeconds(0.8f);
        }
    }

    //Corrutina de segundo ataque:
    public IEnumerator shootSaw()
    {
        while (true)
        {
            Instantiate(saw, new Vector2(GetComponent<Rigidbody2D>().transform.position.x, GetComponent<Rigidbody2D>().transform.position.y -2), transform.rotation);
            yield return new WaitForSeconds(7f);
        }
    }


}
