using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class shooter_enemy : MonoBehaviour
{
    public GameObject bullet; 
    void Start()
    {
        StartCoroutine(shootBullet());
    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "returnlaser")
        {
            enemy_spawner.areShooterEnemy = false;
            player.areEnemies--;

            player.coin += 1 * player.moneyup;
            moneytext.takeMoney = true;
            
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
        }
    }

    //Corrutina para atacar:
    public IEnumerator  shootBullet()
    {
        while (true)
        {
            Instantiate(bullet, new Vector2(GetComponent<Rigidbody2D>().transform.position.x-1f, GetComponent<Rigidbody2D>().transform.position.y-0.3f), transform.rotation);
            yield return new WaitForSeconds(3.0f);
        }
    }


}
