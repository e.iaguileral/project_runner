using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_enemy : MonoBehaviour
{
    public Sprite returnSprite;
    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
      rb = GetComponent<Rigidbody2D>();
      rb.velocity = new Vector2(-13,0);
    }

    // Update is called once per frame
    void Update()
    {
        destroyLeft();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Colisi�n con golpe para cambiar el estado:
        if (collision.transform.tag == "strike")
        {
            rb.velocity = new Vector2(18,0);

            this.GetComponent<SpriteRenderer>().flipX = false;
            this.transform.tag = "returnlaser";

            if (player.laserpower == 1) {
                GetComponent<SpriteRenderer>().sprite = returnSprite;
            }
        }
    }

    //Funci�n para destruir el objeto si se pasa de cierto punto del lado izquiero:
    private void destroyLeft()
    {
        if (rb.transform.position.x <= -10)
        {
            Destroy(this.gameObject);
        }
    }
}
