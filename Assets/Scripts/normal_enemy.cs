using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class normal_enemy : MonoBehaviour
{

    public player player;
    private Rigidbody2D rb;
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-9 + (player.stage * -1), rb.velocity.y);
    }

    void Update()
    {
        destroyLeft();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //El enemigo puede ser golpeado o disparado con el l�ser mejorado:
        if (collision.transform.tag == "strike" || (collision.transform.tag == "returnlaser" && player.laserpower == 1))
        {
            player.coin += 1 * player.moneyup;
            player.areEnemies--;
            moneytext.takeMoney = true;

            //Si ha sido un golpe y tienes el objeto de vampiro:
            if (collision.transform.tag == "strike" && player.vampire) {
                if (player.life < player.maxLife)
                {
                    int numr = Random.Range(1, 8);
                    if (numr == 1) {
                        player.life++;
                        spiny_enemy.playerTakeDamage = true;
                    } 
                }
            }
            Destroy(this.gameObject);
        }
        
    }

    //Funci�n para destruir el objeto si se pasa de cierto punto del lado izquiero:
    private void destroyLeft()
    {
        if (rb.transform.position.x <= -10)
        {
            player.areEnemies--;
            Destroy(this.gameObject);
        }
    }
}
