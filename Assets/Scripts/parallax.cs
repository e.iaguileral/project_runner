using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parallax : MonoBehaviour
{
    public float speed;
    private Vector2 offset;
    private Material material;

    public static bool canparallax = true;
    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<SpriteRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (canparallax) {
            offset = new Vector2(speed + player.stage/9,0) * Time.deltaTime;
            material.mainTextureOffset += offset;
        } 
    }
}
